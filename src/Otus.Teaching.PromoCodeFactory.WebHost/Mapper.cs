﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost;

public static class Mapper
{
    public static EmployeeShortResponse MapToShortResponse(this Employee employee)
    {
        return new EmployeeShortResponse()
        {
            Id = employee.Id,
            Email = employee.Email,
            FullName = employee.FullName,
        };
    }

    public static EmployeeResponse Map(this Employee employee)
    {
        return new EmployeeResponse()
        {
            Id = employee.Id,
            Email = employee.Email,
            Roles = employee.Roles.Select(x => x.Map()).ToList(),
            FullName = employee.FullName,
            AppliedPromocodesCount = employee.AppliedPromocodesCount
        };
    }

    public static RoleItemResponse Map(this Role role)
    {
        return new RoleItemResponse
        {
            Id = role.Id,
            Name = role.Name,
            Description = role.Description
        };
    }

    public static Employee Map(this EmployeeUpdate employeeUpdate, List<Role> roles)
    {
        return new Employee
        {
            FirstName = employeeUpdate.FirstName,
            LastName = employeeUpdate.LastName,
            Email = employeeUpdate.Email,
            Roles = roles,
            AppliedPromocodesCount = employeeUpdate.AppliedPromocodesCount
        };
    }

    public static Employee Map(this EmployeeUpdate employeeUpdate, List<Role> roles, Guid id)
    {
        return new Employee
        {
            Id = id,
            FirstName = employeeUpdate.FirstName,
            LastName = employeeUpdate.LastName,
            Email = employeeUpdate.Email,
            Roles = roles,
            AppliedPromocodesCount = employeeUpdate.AppliedPromocodesCount
        };
    }
}
