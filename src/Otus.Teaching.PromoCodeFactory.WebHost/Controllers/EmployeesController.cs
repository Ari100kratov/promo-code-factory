﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Сотрудники
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class EmployeesController : ControllerBase
{
    private readonly IRepository<Employee> _employeeRepository;
    private readonly IRepository<Role> _roleRepository;

    public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
    {
        _employeeRepository = employeeRepository;
        _roleRepository = roleRepository;
    }

    /// <summary>
    /// Получить данные всех сотрудников
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<EmployeeShortResponse>> Get()
    {
        var employees = await _employeeRepository.GetAllAsync();
        return employees.Select(x => x.MapToShortResponse()).ToList();
    }

    /// <summary>
    /// Получить данные сотрудника по Id
    /// </summary>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<EmployeeResponse>> Get(Guid id)
    {
        var employee = await _employeeRepository.GetByIdAsync(id);
        if (employee is null)
            return NotFound();

        return employee.Map();
    }

    /// <summary>
    /// Добавить сотрудника
    /// </summary>
    [HttpPost]
    public async Task<ActionResult<EmployeeResponse>> Add(EmployeeUpdate employeeUpdate)
    {
        var roles = new List<Role>(employeeUpdate.RoleIds.Count);
        foreach (var roleId in employeeUpdate.RoleIds)
        {
            var role = await _roleRepository.GetByIdAsync(roleId);
            if (role is null)
                return BadRequest($"Role by Id = {roleId} not found");

            roles.Add(role);
        }

        var addedEmployee = await _employeeRepository.AddAsync(employeeUpdate.Map(roles));
        return addedEmployee.Map();
    }

    /// <summary>
    /// Изменить данные о сотруднике
    /// </summary>
    /// <param name="id">Идентификатор изменяемого сотрудника</param>
    [HttpPut("{id:guid}")]
    public async Task<ActionResult<EmployeeResponse>> Edit(Guid id, EmployeeUpdate employeeUpdate)
    {
        var employee = await _employeeRepository.GetByIdAsync(id);
        if (employee is null)
            return BadRequest($"Employee by Id = {id} not found");

        var roles = new List<Role>(employeeUpdate.RoleIds.Count);
        foreach (var roleId in employeeUpdate.RoleIds)
        {
            var role = await _roleRepository.GetByIdAsync(roleId);
            if (role is null)
                return BadRequest($"Role by Id = {roleId} not found");

            roles.Add(role);
        }

        var addedEmployee = await _employeeRepository.EditAsync(employeeUpdate.Map(roles, id));
        return addedEmployee.Map();
    }

    /// <summary>
    /// Удалить сотрудника
    /// </summary>
    /// <param name="id">Идентификатор удаляемого сотрудника</param>
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> Delete(Guid id)
    {
        var employee = await _employeeRepository.GetByIdAsync(id);
        if (employee is null)
            return BadRequest($"Employee by Id = {id} not found");

        await _employeeRepository.DeleteAsync(id);
        return Ok();
    }
}