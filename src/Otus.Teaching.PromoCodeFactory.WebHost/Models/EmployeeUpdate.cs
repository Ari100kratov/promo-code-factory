﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public class EmployeeUpdate
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public List<Guid> RoleIds { get; set; } = new List<Guid>();
    public int AppliedPromocodesCount { get; set; }
}
