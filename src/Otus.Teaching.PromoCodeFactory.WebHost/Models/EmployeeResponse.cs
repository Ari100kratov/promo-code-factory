﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public class EmployeeResponse
{
    public Guid Id { get; set; }
    public string FullName { get; set; } = string.Empty;

    public string Email { get; set; } = string.Empty;

    public List<RoleItemResponse> Roles { get; set; } = new List<RoleItemResponse>();

    public int AppliedPromocodesCount { get; set; }
}