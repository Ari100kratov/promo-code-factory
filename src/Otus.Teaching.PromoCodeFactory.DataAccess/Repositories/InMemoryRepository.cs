﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
{
    private readonly List<T> _data;

    public InMemoryRepository(IEnumerable<T> data)
    {
        _data = data.ToList();
    }

    public Task<IEnumerable<T>> GetAllAsync()
    {
        return Task.FromResult(_data.AsEnumerable());
    }

    public Task<T?> GetByIdAsync(Guid id)
    {
        return Task.FromResult(_data.FirstOrDefault(x => x.Id == id));
    }

    public Task<T> AddAsync(T item)
    {
        if (item is null)
            throw new ArgumentNullException(nameof(item));

        item.Id = Guid.NewGuid();
        _data.Add(item);

        return Task.FromResult(item);
    }

    public Task<T> EditAsync(T item)
    {
        if (item is null)
            throw new ArgumentNullException(nameof(item));

        var editItemIndex = _data.FindIndex(x => x.Id == item.Id);
        if (editItemIndex < 0)
            throw new ArgumentException($"Item with Id = {item.Id} not found in cache");

        _data[editItemIndex] = item;
        return Task.FromResult(item);
    }

    public Task DeleteAsync(Guid id)
    {
        var editItem = _data.FirstOrDefault(x => x.Id == id)
            ?? throw new ArgumentException($"Item with Id = {id} not found in cache");

        _data.Remove(editItem);
        return Task.CompletedTask;
    }
}