﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

public class Role : BaseEntity
{
    public string Name { get; set; } = string.Empty;

    public string? Description { get; set; }
}