﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

public class Employee : BaseEntity
{
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;

    public string FullName => $"{FirstName} {LastName}";

    public string Email { get; set; } = string.Empty;

    public List<Role> Roles { get; set; } = new List<Role>();

    public int AppliedPromocodesCount { get; set; }
}